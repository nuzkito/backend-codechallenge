# Notas
En general me he limitado a usar las características de Laravel. Laravel lo hace todo súper sencillo, así que la complejidad del ejemplo tampoco da para mostrar mucho más. Posiblemente se pueda separar en otras clases algunas cosas como la generación de la clave de la caché, para poder reutilizarla tanto en la generación como en la invalidación de la misma; o el _mapeo_ de los datos de los pedidos al pedírselos a la API. Pero por sencillez me he limitado a dejarlo como está.

## Instalación
```
composer install
touch tests.sqlite
```

## Estructura de la base de datos
En el caso de la dirección del pedido, me he limitado a añadir un campo de texto al pedido, no he creado una nueva tabla en la que guardar las direcciones.

Sobre los clientes, he asumido que ya están creados en la base de datos antes de insertar un pedido.

## Tests
He añadido algunos tests HTTP para ambos _endpoints_ y unitarios para el componente que valida la franja horaria.

## Caché
He implementado caché en la obtención de pedidos. Con un volumen de peticiones algo alto considero que es importante servir los datos devueltos por esta ruta desde una caché.

## Colas
La asignación del _driver_ a un pedido la he hecho en un _job_. Tiene sentido que esta operación no se haga al mismo momento de guardar el pedido, ya que entiendo que en un entorno real con muchos más datos almacenados pueda depender de otros parámetros y se hagan cálculos más complejos, no un simple _random_.
