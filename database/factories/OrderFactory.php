<?php

use App\Order;
use App\Customer;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'telephone' => (string) mt_rand(600000000, 999999999),
        'delivery_address' => $faker->address,
        'delivery_date' => $faker->date,
        'start_delivery_time' => $faker->time,
        'end_delivery_time' => $faker->time,
        'customer_id' => function () {
            return factory(Customer::class)->create()->id;
        },
    ];
});
