<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Rules\TimeInterval;

class TimeIntervalValidationRuleTest extends TestCase
{
    protected function executeRule($start_time, $end_time)
    {
        $rule = new TimeInterval($end_time);

        return $rule->passes('fake attribute', $start_time);
    }

    public function test_validation_pass_with_a_valid_range()
    {
        $this->assertTrue($this->executeRule('11:00', '12:00'));
        $this->assertTrue($this->executeRule('11:00', '19:00'));
        $this->assertTrue($this->executeRule('10:00', '12:00'));
        $this->assertTrue($this->executeRule('14:00', '19:00'));
    }

    public function test_validation_fails_with_a_range_less_than_1_hour()
    {
        $this->assertFalse($this->executeRule('10:00', '10:00'));
    }

    public function test_validation_fails_with_a_range_greater_than_8_hours()
    {
        $this->assertFalse($this->executeRule('10:00', '19:00'));
    }

    public function test_validation_fails_if_times_are_inverted()
    {
        $this->assertFalse($this->executeRule('12:00', '10:00'));
    }
}
